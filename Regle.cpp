/////////////////////////////////////////////////////////////////////////////
//Fichier Regle.cpp
/////////////////////////////////////////////////////////////////////////////
/**
 * \file Regle.cpp
 * \brief Ce fichier contient une implantation des méthodes de la classe Regle
 * \author ?
 * \version 0.5
 * \date mai 2017
 *
 */

#include "Regle.h"

namespace tp1 {
bool Regle::operator==(const Regle & r) const{
	return premisses == r.premisses and conclusions == r.conclusions;
}
bool Regle::operator!=(const Regle & r) const{
	return not(*this == r);
}
}

