/*
 * test_gtest.cpp
 *
 *  Created on: 2017-06-01
 *      Author: etudiant
 */
#include "gtest/gtest.h"
#include "Regle.h"
#include "SystemeExpert.h"
TEST(Regle, defaultConstructor){
	tp1::Regle r{};
	ASSERT_TRUE(r.GetConclusions().empty() and r.GetPremisses().empty());
}

TEST(Regle, ajouterValeurs){
	using namespace tp1;
	Regle r{};
	std::list<TypeFait> & premisses = r.GetPremisses();
	std::list<TypeFait> & conclusions = r.GetConclusions();
	premisses.push_back("prem1");
	premisses.push_back("prem2");

	conclusions.push_back("concl1");
	conclusions.push_back("concl2");

	ASSERT_EQ(r.GetPremisses(), premisses);
	ASSERT_EQ(r.GetConclusions(), conclusions);

	ASSERT_EQ(*(r.GetPremisses().begin()), "prem1");
	ASSERT_EQ(*(++r.GetPremisses().begin()), "prem2");

	ASSERT_EQ(*(r.GetConclusions().begin()), "concl1");
	ASSERT_EQ(*(++r.GetConclusions().begin()), "concl2");

}

TEST(Regle, egalite){
	using namespace tp1;
	Regle vide1;
	Regle vide2;
	ASSERT_EQ(vide1, vide2);

	Regle r1;
	std::list<TypeFait> & premisses1 = r1.GetPremisses();
	std::list<TypeFait> & conclusions1 = r1.GetConclusions();
	premisses1.push_back("prem1");
	premisses1.push_back("prem2");

	conclusions1.push_back("concl1");
	conclusions1.push_back("concl2");

	Regle r2;
	std::list<TypeFait> & premisses2 = r2.GetPremisses();
	std::list<TypeFait> & conclusions2 = r2.GetConclusions();
	premisses2.push_back("prem1");
	premisses2.push_back("prem2");

	conclusions2.push_back("concl1");
	conclusions2.push_back("concl2");

	ASSERT_EQ(r1,r2);
	ASSERT_NE(r1,vide1);

	conclusions2.push_back("vagin");

	ASSERT_NE(r1,r2);
}
TEST(Regle, copieConstructeur){
	using namespace tp1;
	Regle vide1;
	Regle vide2 = vide1;

	Regle r1;
	std::list<TypeFait> & premisses1 = r1.GetPremisses();
	std::list<TypeFait> & conclusions1 = r1.GetConclusions();
	premisses1.push_back("prem1");
	premisses1.push_back("prem2");

	conclusions1.push_back("concl1");
	conclusions1.push_back("concl2");

	Regle r2 = r1;
	ASSERT_EQ(r1,r2);
}
TEST(Regle, opAssignation){
	using namespace tp1;
	Regle vide1;
	Regle r2;
	Regle r1;

	std::list<TypeFait> & premisses1 = r1.GetPremisses();
	std::list<TypeFait> & conclusions1 = r1.GetConclusions();
	premisses1.push_back("prem1");
	premisses1.push_back("prem2");

	conclusions1.push_back("concl1");
	conclusions1.push_back("concl2");
	ASSERT_NE(r2,r1);
	r2 = r1;
	ASSERT_EQ(r2, r1);
	r2 = vide1;
	ASSERT_NE(r2, r1);
}
TEST(SE, chargerSE){
	using namespace tp1;
	std::ifstream ifs{"Animaux.txt"};
	SystemeExpert se;
	se.chargerSE(ifs);
}



