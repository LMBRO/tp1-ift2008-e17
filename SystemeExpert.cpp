/**
 * \file SystemeExpert.cpp
 * \brief Ce fichier contient une implantation des méthodes de la classe SystemeExpert
 * \author ?
 * \version 0.5
 * \date juin 2017
 *
 */

#include "SystemeExpert.h"

namespace tp1
{
std::string const FIN_REGLES = "!!";
std::string const FIN_PREM = "!>";
std::string const FIN_REGLE = "!%";
void SystemeExpert::ajouterRegleSE(const Regle & tr){
	baseRegles.ajouter(tr, baseRegles.taille() + 1);
}
void SystemeExpert::ajouterFaitSE(const TypeFait & tf){
	baseFaits.push_back(tf);
}

bool SystemeExpert::lireAjouterProchaineRegle(std::ifstream& EntreeFichier){
	ASSERTION(EntreeFichier.good());
	using std::string;
	using std::getline;
	string ligne;
	getline(EntreeFichier, ligne);
	if ( ligne == FIN_REGLES){
		return false;
	}
	Regle r{};
	std::list<TypeFait> & premisses = r.GetPremisses();
	std::list<TypeFait> & conclusions = r.GetConclusions();
	while(ligne != FIN_PREM){
		if(ligne == FIN_REGLE or ligne == FIN_REGLES){
			throw std::invalid_argument("Format invalide.");
		}
		premisses.push_back(ligne);
		getline(EntreeFichier, ligne);
	}
	getline(EntreeFichier, ligne);
	while(ligne != FIN_REGLE and ligne != FIN_REGLES){
		conclusions.push_back(ligne);
		getline(EntreeFichier, ligne);
	}
	SystemeExpert::ajouterRegleSE(r);
	if (ligne == FIN_REGLES)
		return true;
	else
		return false;
}
void SystemeExpert::chargerSE(std::ifstream& EntreeFichier) {
	ASSERTION(EntreeFichier.good());
	using std::string;
	using std::getline;
	using std::cout;
	using std::endl;
	SystemeExpert copy = *this;
	baseRegles = ListeCirculaire<Regle>{};
	baseFaits = std::list<TypeFait>{};
	try
	{

		bool fin_regles = lireAjouterProchaineRegle(EntreeFichier);
		while(!fin_regles){
			fin_regles = lireAjouterProchaineRegle(EntreeFichier);
		}
	}
	catch(const std::invalid_argument & e)
	{
		*this = copy;
		cout << e.what()
		<< endl
		<< "Annulation du chargement, l'ancien système expert a été restauré.";
		ASSERTION("Erreur de chargement");
	}
	//lecture des faits
	string ligne;
	while(getline(EntreeFichier,ligne)){
		ajouterFaitSE(ligne);
	}

}

void SystemeExpert::sauvegarderSE(std::ofstream& sortieFichier) const {
	using std::endl;
	for(int i = 1;i != baseRegles.taille() + 1; ++i){
		Regle r = baseRegles.element(i);
		for(auto itPrem = r.GetPremisses().cbegin(); itPrem != r.GetPremisses().cend();
				++itPrem)
		{
			sortieFichier << *itPrem << endl;
		}
		sortieFichier << FIN_PREM << endl;
		for(auto itConcl = r.GetConclusions().cbegin(); itConcl != r.GetConclusions().cend();
				++itConcl)
		{
			sortieFichier << *itConcl << endl;
		}
		if (i != baseRegles.taille())
			sortieFichier << FIN_REGLE << endl;
	}
	sortieFichier << FIN_REGLES << endl;
	if(baseFaits.empty())
		return;
	auto itFaits = baseFaits.cbegin();
	for(;itFaits != --baseFaits.cend();++itFaits){
		sortieFichier << *itFaits << endl;
	}
	sortieFichier << *itFaits; //pour éviter une ligne vide à la fin du fichier.
}
void SystemeExpert::chainageAvant(ListeCirculaire<Regle>& er) {
	int i = 1;
	while(i < baseRegles.taille() + 1){ //pour chaque regle on verifie les premisses
		Regle & r = baseRegles.element(i);
		bool toutes_les_premisses_dans_faits = true;
		for(TypeFait const & prem : r.GetPremisses()){
			bool premisse_est_dans_faits = false;
			for(TypeFait const & fait : baseFaits){
				if(prem == fait){
					premisse_est_dans_faits = true;
					break;
				}
			}
			if(not premisse_est_dans_faits){
				toutes_les_premisses_dans_faits = false;
				break;
			}
				//une des premisse de la regle n'est pas dans les faits
					   //on continue avec la prochaine regle.
		}
		if(not toutes_les_premisses_dans_faits){
			++i;
			continue;
		}
		//Si l'exécution arrive ici, c'est que toutes les prémisses de la règle
		//sont dans les faits.
		bool nouveau_faits_ajoutees = false;

		for(TypeFait const & concl: r.GetConclusions()){
			bool concl_deja_dans_faits = false;
			for(TypeFait const & fait:baseFaits){
				if(concl == fait){
					concl_deja_dans_faits = true;
					break;
				}
			}
			if(not concl_deja_dans_faits){
				baseFaits.push_back(concl);
				nouveau_faits_ajoutees = true;
			}
		}
		if(nouveau_faits_ajoutees){
			er.ajouter(r,1);
			i = 1;
		}
		else{
			++i;
		}
	}
}
}

